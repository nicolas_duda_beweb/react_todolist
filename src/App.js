import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './components/layout/Header';
import Todos from './components/Todos';
import AddTodo from './components/AddTodo';
import About from './components/pages/About';
import './App.css';
import { useState } from 'react';
import {v4 as uuid} from 'uuid';

function App() {
  const [todos, setTodos] = useState([
    {
      id: uuid(),
      title: 'Take out the trash',
      completed : false
    },
    {
      id: uuid(),
      title: 'Dinner with friends',
      completed : false
    },
    {
      id: uuid(),
      title: 'Meeting with boss',
      completed : false
    }
  ])

  // Toggle complete
  const markComplete = id => {
    setTodos(todos.map(todo => {
      if (todo.id === id) {
        todo.completed = !todo.completed
      }
      return todo;
    }))
  }

  // Delete Todo
  const delTodo = id => {
    setTodos([...todos].filter(todo => todo.id !== id))
  }

  // Add Todo
  const addTodo = title => {
    const newTodo = {
      id: uuid(),
      title,
      completed: false
    }
    setTodos([...todos, newTodo])
  }

  return (
    <Router>
      <div className="App">
        <div className='container'>
          <Header />
          <Route exact path='/' render={props => (
            <React.Fragment>
              <AddTodo addTodo={addTodo}/>
              <Todos todos={todos} markComplete={markComplete} delTodo={delTodo}/>
            </React.Fragment>
          )} />
          <Route path='/about' component={About} />       
        </div>
      </div>
    </Router>
  );
}

export default App;
